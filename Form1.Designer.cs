﻿namespace OracleBlobEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblState = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.bsOracle = new System.Windows.Forms.BindingSource(this.components);
            this.gvOracle = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.bsOracle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOracle)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(3, 10);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 13);
            this.lblState.TabIndex = 0;
            this.lblState.Text = "label1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(44, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // gvOracle
            // 
            this.gvOracle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvOracle.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gvOracle.Location = new System.Drawing.Point(0, 78);
            this.gvOracle.Name = "gvOracle";
            this.gvOracle.Size = new System.Drawing.Size(643, 444);
            this.gvOracle.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblState);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(643, 72);
            this.panel1.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 522);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gvOracle);
            this.Name = "Form1";
            this.Text = "Oracle Blob Editor";
            ((System.ComponentModel.ISupportInitialize)(this.bsOracle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOracle)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource bsOracle;
        private System.Windows.Forms.DataGridView gvOracle;
        private System.Windows.Forms.Panel panel1;
    }
}

