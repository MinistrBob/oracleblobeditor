﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OracleClient;

namespace OracleBlobEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string GenerateConnectionString()
        {
            return "Data Source=( DESCRIPTION = ( ADDRESS_LIST = ( ADDRESS = ( PROTOCOL = TCP )( HOST = {Insert Host Here} )( PORT = {Insert Port Here} ) ) )( CONNECT_DATA = ( SERVER = DEDICATED )( SERVICE_NAME = {Service Name Here } ) ) ); User Id= {DB ID Here}; Password = {Password Here};";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                using (OracleConnection connection = new OracleConnection(GenerateConnectionString()))
                {
                    connection.Open();
                    lblState.Text = connection.State.ToString();

                    OracleCommand oc = connection.CreateCommand();
                    oc.CommandText = "SELECT * FROM {Your Table Here}";

                    OracleDataReader reader = oc.ExecuteReader();

                    bsOracle.DataSource = reader;
                    gvOracle.DataSource = bsOracle;

                    gvOracle.BorderStyle = BorderStyle.Fixed3D;
                    gvOracle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show( "Exception: " + ex.Message );
                lblState.Text = ex.Message;
            }
        }
        
    }
}
